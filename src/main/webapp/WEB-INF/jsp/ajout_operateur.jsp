<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>   
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>OMDATA | Ajout Operateur</title>

<link href="../../webjars/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
 <script src="../../webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>
 <script src="../../webjars/jquery/3.0.0/js/jquery.min.js"></script>
</head>
<body>
	
	<spring:url value="/operateur/ajout_operateur/" var="ajoutURL" />			
			
			<h2>EXPLOITANT</h2>
<form:form modelAttribute="ajout_operateur_form" method="POST" action="$(ajoutURL)" cssClass="form">		
<form:hidden path="CODEEXPL"/>	
			
	<div class="form-group">
  		<label for="usr">Name:</label>
  		<form:input path="CODETYP" cssClass="form-control" id="codetyp"/>
	</div>
	
	<div class="form-group">
  		<label for="pwd">Password:</label>
  		<form:input path="NOMEXPLOITANT" cssClass="form-control" id="nomexpl" />
	</div>
	
	<div class="form-group">
  		<label for="pwd">Password:</label>
  		<form:input path="PRENOMEXPL" cssClass="form-control" id="prenomexpl" />
	</div>
	
	<div class="form-group">
  		<label for="pwd">Password:</label>
  		<form:input path="DATENAISS" cssClass="form-control" id="datenaiss" />
	</div>
	
	<div class="form-group">
  		<label for="pwd">Password:</label>
  		<form:input path="SEXE" cssClass="form-control" id="sexe" />
	</div>
	
	<div class="form-group">
  		<label for="pwd">Password:</label>
  		<form:input path="AGE" cssClass="form-control" id="age" />
	</div>
	
	<div class="form-group">
  		<label for="pwd">Password:</label>
  		<form:input path="ADRESSE" cssClass="form-control" id="adresse" />
	</div>
	
	<div class="form-group">
  		<label for="pwd">Password:</label>
  		<form:input path="NUMTEL" cssClass="form-control" id="numtel" />
	</div>
	
	<div class="form-group">
  		<label for="pwd">Password:</label>
  		<form:input path="NUMCIN" cssClass="form-control" id="numcin" />
	</div>
	
	<div class="form-group">
  		<label for="pwd">Password:</label>
  		<form:input path="DATECIN" cssClass="form-control" id="datecin" />
	</div>
	 	
	<div class="form-group">
  		<label for="pwd">Password:</label>
  		<form:input path="LIEUCIN" cssClass="form-control" id="lieucin" />
	</div> 	
	
	<div class="form-group">
  		<label for="pwd">Password:</label>
  		<form:input path="COMMUNE" cssClass="form-control" id="commune" />
	</div> 	
	
	<div class="form-group">
  		<label for="pwd">Password:</label>
  		<form:input path="NIF" cssClass="form-control" id="nif" />
	</div> 	
	
	<Button type="submit" class="btn btn-danger">ENREGISTRER</Button>
	
	
</form:form>
			
</body>
</html>