package org.mmp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.mmp.entity.Operateur;
import org.mmp.service.OperateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Controller
public class OperateurController {
	
	@Autowired
	OperateurService operateurService;
	
	
	@RequestMapping(value="/list_operateur", method=RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView model = new ModelAndView("operateur_list");
		List<Operateur> operateur_list = operateurService.getAllOperateur();
		model.addObject("operateur_list",operateur_list);
			
		return model;
	}
	
	@RequestMapping(value="/ajout_operateur", method=RequestMethod.GET)
	public ModelAndView ajout_operateur() {
		ModelAndView model = new ModelAndView("list_operateur");
		Operateur op = new Operateur();
		model.addObject("ajout_operateur_form",op);
		model.setViewName("ajout_operateur");
			
		return model;
	}
	
	@RequestMapping(value="/edit_operateur/{CODEEXPL}", method=RequestMethod.GET)
	public ModelAndView edit_operateur(@PathVariable int CODEEXPL ) {
		ModelAndView model = new ModelAndView();
		Operateur op = operateurService.getOperateurById(CODEEXPL);
		model.addObject("ajout_operateur_form",op);
		model.setViewName("ajout_operateur");
			
		return model;
	}

	@RequestMapping(value="/ajout_operateur/", method=RequestMethod.POST)
	public ModelAndView enreg_operateur(@ModelAttribute("ajout_operateur_form") Operateur operateur ) {
		operateurService.saveOrUpdate(operateur);
		
		return new ModelAndView("redirect:/operateur/list_operateur");
	}
		
	@RequestMapping(value="/suppr_operateur/{CODEEXPL}", method=RequestMethod.POST)
	public ModelAndView suppr_operateur(@PathVariable("CODEEXPL") int CODEEXPL) {
		operateurService.deleteOperateur(CODEEXPL);
		
		return new ModelAndView("redirect:/operateur/list_operateur");
	}
	
	
}
