package org.mmp.repository;

import org.mmp.entity.Operateur;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OperateurRepository extends CrudRepository<Operateur, Integer>{
	
	
}
