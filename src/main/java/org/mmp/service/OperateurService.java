package org.mmp.service;

import java.util.List;

import org.mmp.entity.Operateur;


public interface OperateurService {
	
	public List<Operateur> getAllOperateur();
	
	public Operateur getOperateurById(int CODEEXPL);
	
	public void saveOrUpdate(Operateur operateur);
	
	public void deleteOperateur(int CODEEXPL);
	
	
}
