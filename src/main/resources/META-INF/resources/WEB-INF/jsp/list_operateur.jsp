<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>OMDATA | LISTE OPERATEUR</title>
<link href="../../webjars/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
 <script src="../../webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>
 <script src="../../webjars/jquery/3.0.0/js/jquery.min.js"></script>
</head>
<body>
		
		<div class="container">
  <h2>Striped Rows</h2>
  <p>The .table-striped class adds zebra-stripes to a table:</p>            
  <table class="table table-striped">
    <thead>
      <tr>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th>6</th>
        <th>7</th>
        <th>8</th>
        <th>9</th>
        <th>10</th>
        <th>11</th>
        <th>12</th>
        <th>13</th>
        <th>14</th>
      </tr>
    </thead>
    
    
    <tbody>
      	<c:forEach items="$(operateur_list)"  var="operateur">
  			<tr>
  				<td>
  					${operateur.CODEEXPL}
  				</td>
  				<td>
  					${operateur.CODETYP}
  				</td>
  				<td>
  					${operateur.NOMEXPLOITANT}
  				</td>
  				<td>
  					${operateur.PRENOMEXPL}
  				</td>
  				<td>
  					${operateur.DATENAISS}
  				</td>
  				<td>
  					${operateur.SEXE}
  				</td>
  				<td>
  					${operateur.AGE}
  				</td>
  				<td>
  					${operateur.ADRESSE}
  				</td>
  				<td>
  					${operateur.NUMTEL}
  				</td>
  				<td>
  					${operateur.NUMCIN}
  				</td>
  				<td>
  					${operateur.DATECIN}
  				</td>
  				<td>
  					${operateur.LIEUCIN}
  				</td>
  				<td>
  					${operateur.COMMUNE}
  				</td>
  				<td>
  					${operateur.NIF}
  				</td>
  				
  				<td>
  					<spring:url value="/operateur/edit_operateur/${operateur.CODEEXPL}" var="updateURL" />  				 
  					<a class="btn btn-danger" href="$(updateURL)" role="button">EDITER</a>
  				</td>
  				
  				<td>
  					<spring:url value="/operateur/suppr_operateur/${operateur.CODEEXPL}" var="deleteURL" />  				 
  					<a class="btn btn-primary" href="$(deleteURL)" role="button">SUPPRIMER</a>
  				</td>	
  			</tr>    		
      	</c:forEach>
    </tbody>
    
  </table>
		
		<td>
  			<spring:url value="/operateur/ajout_operateur" var="ajoutURL" />  				 
  			<a class="btn btn-success" href="$(ajoutURL)" role="button">AJOUTER</a>
  		</td>
		
</div>	
</body>
</html>